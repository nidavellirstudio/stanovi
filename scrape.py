from bs4 import BeautifulSoup
import helpers as helpers
import nidavellir_mail as mailer


def scrape_njuskalo():
    njuskalo = helpers.simple_get('http://www.njuskalo.hr/iznajmljivanje-stanova/sisak')
    njuska = BeautifulSoup(njuskalo, 'html.parser')
    njuskaloBaseLink = 'https://njuskalo.hr'
    oglasi = {}
    with open('njuskalo.csv', 'r') as fin:
        for line in fin:
            stan = line.split(";")
            oglasi[stan[0]] =  stan[1]

    for oglas in njuska.select('.EntityList-items .EntityList-item--Regular'):
        title = oglas.select('.entity-title a')
        if len(title) > 0:
            link = title[0]
            naslov = link.text
            href = link.attrs['href']
            if(naslov not in oglasi):
                mailer.send_message("""
                Novi stan je slobodan
                Naziv: %s
                Url: %s%s
                """ % (naslov, njuskaloBaseLink, href) )
                oglasi[naslov] = href
                helpers.appendTo('njuskalo.csv', naslov, href)
    return oglasi

def scrape_index():
    index = helpers.simple_get('https://www.index.hr/oglasi/najam-stanova/gid/3279?pojam=&sortby=1&elementsNum=10&cijenaod=0&cijenado=350000&tipoglasa=1&pojamZup=1163&grad=1533&naselje=&attr_Int_988=&attr_Int_887=&attr_bit_stan=&attr_bit_brojEtaza=&attr_gr_93_1=&attr_gr_93_2=&attr_Int_978=&attr_Int_1334=&attr_bit_eneregetskiCertifikat=&vezani_na=988-887_562-563_978-1334')
    index_html = BeautifulSoup(index, 'html.parser')
    oglasi = {}
    with open('index.csv', 'r+') as fin:
        for line in fin:
            stan = line.split(";")
            oglasi[stan[0]] =  stan[1]

    for link in index_html.select('a.result'):
        href = link.attrs['href']
        naslov_elem = link.select('.title')
        if(len(naslov_elem) > 0):
            title_elem = naslov_elem[0]
            naslov = title_elem.text.strip()
            if(naslov not in oglasi):
                helpers.appendTo('index.csv', naslov, href)
                mailer.send_message("""
                Novi stan je slobodan
                Naziv: %s
                Url: %s
                """ % (naslov, href) )
                oglasi[naslov] = href
    return oglasi