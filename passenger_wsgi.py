import os
import sys
import scrape as scrape
import helpers

sys.path.insert(0, os.path.dirname(__file__))


def application(environ, start_response):
    start_response('200 OK', [('Content-Type', 'text/plain')])
    scrape.scrape_njuskalo()
    response = helpers.readTheFile('njuskalo.csv')
    scrape.scrape_index()
    response += helpers.readTheFile('index.csv')
    return [response.encode()]
